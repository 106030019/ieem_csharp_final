﻿namespace 簡易資金管理工具
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmB_addcounty = new System.Windows.Forms.ComboBox();
            this.rdb_f = new System.Windows.Forms.RadioButton();
            this.rdb_m = new System.Windows.Forms.RadioButton();
            this.cmbB_job4 = new System.Windows.Forms.ComboBox();
            this.cmbB_job3 = new System.Windows.Forms.ComboBox();
            this.lbl10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbB_job2 = new System.Windows.Forms.ComboBox();
            this.cmbB_job1 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_ok = new System.Windows.Forms.Button();
            this.lbl9 = new System.Windows.Forms.Label();
            this.txtb_cmp_phone = new System.Windows.Forms.TextBox();
            this.txtb_cmp_email = new System.Windows.Forms.TextBox();
            this.lbl8 = new System.Windows.Forms.Label();
            this.txtb_address = new System.Windows.Forms.TextBox();
            this.cmb_address = new System.Windows.Forms.ComboBox();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.txtb_name = new System.Windows.Forms.TextBox();
            this.lbl5 = new System.Windows.Forms.Label();
            this.txtb_good = new System.Windows.Forms.TextBox();
            this.lbl4 = new System.Windows.Forms.Label();
            this.cmb_county = new System.Windows.Forms.ComboBox();
            this.lbl3 = new System.Windows.Forms.Label();
            this.txtb_cmp_num = new System.Windows.Forms.TextBox();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.txtb_cmp_name = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.customerBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TaxEnabled = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.explainBox = new System.Windows.Forms.TextBox();
            this.moneyBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.detailCombo = new System.Windows.Forms.ComboBox();
            this.CategoryCom = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.taxPer = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.dateBtn = new System.Windows.Forms.Button();
            this.dateBox = new System.Windows.Forms.TextBox();
            this.totalList = new System.Windows.Forms.ListBox();
            this.allViewList = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.InType = new System.Windows.Forms.RadioButton();
            this.OutType = new System.Windows.Forms.RadioButton();
            this.OkBtn = new System.Windows.Forms.Button();
            this.btn_export2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(901, 504);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cmB_addcounty);
            this.tabPage1.Controls.Add(this.rdb_f);
            this.tabPage1.Controls.Add(this.rdb_m);
            this.tabPage1.Controls.Add(this.cmbB_job4);
            this.tabPage1.Controls.Add(this.cmbB_job3);
            this.tabPage1.Controls.Add(this.lbl10);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.cmbB_job2);
            this.tabPage1.Controls.Add(this.cmbB_job1);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.btn_ok);
            this.tabPage1.Controls.Add(this.lbl9);
            this.tabPage1.Controls.Add(this.txtb_cmp_phone);
            this.tabPage1.Controls.Add(this.txtb_cmp_email);
            this.tabPage1.Controls.Add(this.lbl8);
            this.tabPage1.Controls.Add(this.txtb_address);
            this.tabPage1.Controls.Add(this.cmb_address);
            this.tabPage1.Controls.Add(this.lbl7);
            this.tabPage1.Controls.Add(this.lbl6);
            this.tabPage1.Controls.Add(this.txtb_name);
            this.tabPage1.Controls.Add(this.lbl5);
            this.tabPage1.Controls.Add(this.txtb_good);
            this.tabPage1.Controls.Add(this.lbl4);
            this.tabPage1.Controls.Add(this.cmb_county);
            this.tabPage1.Controls.Add(this.lbl3);
            this.tabPage1.Controls.Add(this.txtb_cmp_num);
            this.tabPage1.Controls.Add(this.lbl2);
            this.tabPage1.Controls.Add(this.lbl);
            this.tabPage1.Controls.Add(this.txtb_cmp_name);
            this.tabPage1.Controls.Add(this.lbl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage1.Size = new System.Drawing.Size(893, 475);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "公司資料";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cmB_addcounty
            // 
            this.cmB_addcounty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmB_addcounty.FormattingEnabled = true;
            this.cmB_addcounty.Location = new System.Drawing.Point(252, 211);
            this.cmB_addcounty.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmB_addcounty.Name = "cmB_addcounty";
            this.cmB_addcounty.Size = new System.Drawing.Size(108, 23);
            this.cmB_addcounty.TabIndex = 33;
            // 
            // rdb_f
            // 
            this.rdb_f.AutoSize = true;
            this.rdb_f.Location = new System.Drawing.Point(699, 170);
            this.rdb_f.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdb_f.Name = "rdb_f";
            this.rdb_f.Size = new System.Drawing.Size(43, 19);
            this.rdb_f.TabIndex = 32;
            this.rdb_f.TabStop = true;
            this.rdb_f.Text = "女";
            this.rdb_f.UseVisualStyleBackColor = true;
            // 
            // rdb_m
            // 
            this.rdb_m.AutoSize = true;
            this.rdb_m.Location = new System.Drawing.Point(616, 170);
            this.rdb_m.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdb_m.Name = "rdb_m";
            this.rdb_m.Size = new System.Drawing.Size(43, 19);
            this.rdb_m.TabIndex = 31;
            this.rdb_m.TabStop = true;
            this.rdb_m.Text = "男";
            this.rdb_m.UseVisualStyleBackColor = true;
            // 
            // cmbB_job4
            // 
            this.cmbB_job4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbB_job4.FormattingEnabled = true;
            this.cmbB_job4.IntegralHeight = false;
            this.cmbB_job4.Location = new System.Drawing.Point(616, 417);
            this.cmbB_job4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbB_job4.Name = "cmbB_job4";
            this.cmbB_job4.Size = new System.Drawing.Size(203, 23);
            this.cmbB_job4.TabIndex = 30;
            // 
            // cmbB_job3
            // 
            this.cmbB_job3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbB_job3.FormattingEnabled = true;
            this.cmbB_job3.Location = new System.Drawing.Point(129, 412);
            this.cmbB_job3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbB_job3.Name = "cmbB_job3";
            this.cmbB_job3.Size = new System.Drawing.Size(264, 23);
            this.cmbB_job3.TabIndex = 29;
            this.cmbB_job3.SelectedIndexChanged += new System.EventHandler(this.cmbB_job3_SelectedIndexChanged);
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(508, 419);
            this.lbl10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(92, 15);
            this.lbl10.TabIndex = 28;
            this.lbl10.Text = "行業別(細類)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(24, 415);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 15);
            this.label14.TabIndex = 27;
            this.label14.Text = "行業別(小類)";
            // 
            // cmbB_job2
            // 
            this.cmbB_job2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbB_job2.FormattingEnabled = true;
            this.cmbB_job2.Location = new System.Drawing.Point(616, 372);
            this.cmbB_job2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbB_job2.Name = "cmbB_job2";
            this.cmbB_job2.Size = new System.Drawing.Size(203, 23);
            this.cmbB_job2.TabIndex = 26;
            this.cmbB_job2.SelectedIndexChanged += new System.EventHandler(this.cmbB_job2_SelectedIndexChanged);
            // 
            // cmbB_job1
            // 
            this.cmbB_job1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbB_job1.FormattingEnabled = true;
            this.cmbB_job1.Location = new System.Drawing.Point(129, 367);
            this.cmbB_job1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbB_job1.Name = "cmbB_job1";
            this.cmbB_job1.Size = new System.Drawing.Size(264, 23);
            this.cmbB_job1.TabIndex = 25;
            this.cmbB_job1.SelectedIndexChanged += new System.EventHandler(this.cmbB_job1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(508, 378);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 15);
            this.label12.TabIndex = 24;
            this.label12.Text = "行業別(中類)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 370);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 15);
            this.label11.TabIndex = 22;
            this.label11.Text = "行業別(大類)";
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(170, 18);
            this.btn_ok.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(67, 28);
            this.btn_ok.TabIndex = 21;
            this.btn_ok.Text = "儲存";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.Location = new System.Drawing.Point(534, 308);
            this.lbl9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(67, 15);
            this.lbl9.TabIndex = 20;
            this.lbl9.Text = "公司電話";
            // 
            // txtb_cmp_phone
            // 
            this.txtb_cmp_phone.Location = new System.Drawing.Point(612, 306);
            this.txtb_cmp_phone.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_cmp_phone.Name = "txtb_cmp_phone";
            this.txtb_cmp_phone.Size = new System.Drawing.Size(132, 25);
            this.txtb_cmp_phone.TabIndex = 19;
            // 
            // txtb_cmp_email
            // 
            this.txtb_cmp_email.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtb_cmp_email.Location = new System.Drawing.Point(129, 299);
            this.txtb_cmp_email.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_cmp_email.Name = "txtb_cmp_email";
            this.txtb_cmp_email.Size = new System.Drawing.Size(132, 25);
            this.txtb_cmp_email.TabIndex = 18;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Location = new System.Drawing.Point(37, 302);
            this.lbl8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(68, 15);
            this.lbl8.TabIndex = 17;
            this.lbl8.Text = "公司email";
            // 
            // txtb_address
            // 
            this.txtb_address.Location = new System.Drawing.Point(27, 253);
            this.txtb_address.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_address.Name = "txtb_address";
            this.txtb_address.Size = new System.Drawing.Size(530, 25);
            this.txtb_address.TabIndex = 16;
            // 
            // cmb_address
            // 
            this.cmb_address.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_address.FormattingEnabled = true;
            this.cmb_address.Location = new System.Drawing.Point(120, 211);
            this.cmb_address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_address.Name = "cmb_address";
            this.cmb_address.Size = new System.Drawing.Size(108, 23);
            this.cmb_address.TabIndex = 15;
            this.cmb_address.SelectedIndexChanged += new System.EventHandler(this.cmb_address_SelectedIndexChanged);
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Location = new System.Drawing.Point(24, 213);
            this.lbl7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(67, 15);
            this.lbl7.TabIndex = 14;
            this.lbl7.Text = "公司地址";
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Location = new System.Drawing.Point(519, 170);
            this.lbl6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(82, 15);
            this.lbl6.TabIndex = 11;
            this.lbl6.Text = "負責人性別";
            // 
            // txtb_name
            // 
            this.txtb_name.Location = new System.Drawing.Point(120, 164);
            this.txtb_name.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_name.Name = "txtb_name";
            this.txtb_name.Size = new System.Drawing.Size(132, 25);
            this.txtb_name.TabIndex = 10;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(24, 167);
            this.lbl5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(82, 15);
            this.lbl5.TabIndex = 9;
            this.lbl5.Text = "負責人姓名";
            // 
            // txtb_good
            // 
            this.txtb_good.Location = new System.Drawing.Point(616, 119);
            this.txtb_good.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_good.Name = "txtb_good";
            this.txtb_good.Size = new System.Drawing.Size(132, 25);
            this.txtb_good.TabIndex = 8;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(466, 130);
            this.lbl4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(131, 15);
            this.lbl4.TabIndex = 7;
            this.lbl4.Text = "主要產品/營業項目";
            // 
            // cmb_county
            // 
            this.cmb_county.DropDownHeight = 200;
            this.cmb_county.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_county.FormattingEnabled = true;
            this.cmb_county.IntegralHeight = false;
            this.cmb_county.Location = new System.Drawing.Point(129, 119);
            this.cmb_county.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmb_county.Name = "cmb_county";
            this.cmb_county.Size = new System.Drawing.Size(108, 23);
            this.cmb_county.TabIndex = 6;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(24, 122);
            this.lbl3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(67, 15);
            this.lbl3.TabIndex = 5;
            this.lbl3.Text = "所在縣市";
            // 
            // txtb_cmp_num
            // 
            this.txtb_cmp_num.Location = new System.Drawing.Point(611, 73);
            this.txtb_cmp_num.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_cmp_num.Name = "txtb_cmp_num";
            this.txtb_cmp_num.Size = new System.Drawing.Size(132, 25);
            this.txtb_cmp_num.TabIndex = 4;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(502, 78);
            this.lbl2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(97, 15);
            this.lbl2.TabIndex = 3;
            this.lbl2.Text = "公司統一編號";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(24, 25);
            this.lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(97, 15);
            this.lbl.TabIndex = 2;
            this.lbl.Text = "公司基本資料";
            // 
            // txtb_cmp_name
            // 
            this.txtb_cmp_name.Location = new System.Drawing.Point(120, 72);
            this.txtb_cmp_name.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtb_cmp_name.Name = "txtb_cmp_name";
            this.txtb_cmp_name.Size = new System.Drawing.Size(132, 25);
            this.txtb_cmp_name.TabIndex = 1;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(24, 72);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(67, 15);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "公司名稱";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.customerBox);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.TaxEnabled);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.explainBox);
            this.tabPage2.Controls.Add(this.moneyBox);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.detailCombo);
            this.tabPage2.Controls.Add(this.CategoryCom);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.taxPer);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.monthCalendar1);
            this.tabPage2.Controls.Add(this.dateBtn);
            this.tabPage2.Controls.Add(this.dateBox);
            this.tabPage2.Controls.Add(this.totalList);
            this.tabPage2.Controls.Add(this.allViewList);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.OkBtn);
            this.tabPage2.Controls.Add(this.btn_export2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tabPage2.Size = new System.Drawing.Size(893, 475);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "日記帳管理";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // customerBox
            // 
            this.customerBox.Location = new System.Drawing.Point(478, 56);
            this.customerBox.Name = "customerBox";
            this.customerBox.Size = new System.Drawing.Size(109, 25);
            this.customerBox.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(435, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 38;
            this.label5.Text = "廠商";
            // 
            // TaxEnabled
            // 
            this.TaxEnabled.AutoSize = true;
            this.TaxEnabled.Location = new System.Drawing.Point(354, 59);
            this.TaxEnabled.Name = "TaxEnabled";
            this.TaxEnabled.Size = new System.Drawing.Size(59, 19);
            this.TaxEnabled.TabIndex = 37;
            this.TaxEnabled.Text = "計稅";
            this.TaxEnabled.UseVisualStyleBackColor = true;
            this.TaxEnabled.CheckStateChanged += new System.EventHandler(this.TaxEnabled_CheckStateChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 35;
            this.label4.Text = "說明";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 15);
            this.label2.TabIndex = 34;
            this.label2.Text = "項目細項說明";
            // 
            // explainBox
            // 
            this.explainBox.Location = new System.Drawing.Point(58, 111);
            this.explainBox.Name = "explainBox";
            this.explainBox.Size = new System.Drawing.Size(447, 25);
            this.explainBox.TabIndex = 25;
            // 
            // moneyBox
            // 
            this.moneyBox.Location = new System.Drawing.Point(66, 56);
            this.moneyBox.Name = "moneyBox";
            this.moneyBox.Size = new System.Drawing.Size(100, 25);
            this.moneyBox.TabIndex = 24;
            this.moneyBox.TextChanged += new System.EventHandler(this.moneyBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 23;
            this.label3.Text = "總金額";
            // 
            // detailCombo
            // 
            this.detailCombo.FormattingEnabled = true;
            this.detailCombo.Location = new System.Drawing.Point(348, 13);
            this.detailCombo.Name = "detailCombo";
            this.detailCombo.Size = new System.Drawing.Size(266, 23);
            this.detailCombo.TabIndex = 22;
            // 
            // CategoryCom
            // 
            this.CategoryCom.FormattingEnabled = true;
            this.CategoryCom.Location = new System.Drawing.Point(81, 13);
            this.CategoryCom.Name = "CategoryCom";
            this.CategoryCom.Size = new System.Drawing.Size(121, 23);
            this.CategoryCom.TabIndex = 21;
            this.CategoryCom.SelectedIndexChanged += new System.EventHandler(this.CategoryCom_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 20;
            this.label1.Text = "項目種類";
            // 
            // taxPer
            // 
            this.taxPer.Location = new System.Drawing.Point(247, 56);
            this.taxPer.Name = "taxPer";
            this.taxPer.Size = new System.Drawing.Size(100, 25);
            this.taxPer.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(204, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 15);
            this.label10.TabIndex = 32;
            this.label10.Text = "稅率";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(599, 78);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 30;
            this.monthCalendar1.Visible = false;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // dateBtn
            // 
            this.dateBtn.Location = new System.Drawing.Point(749, 6);
            this.dateBtn.Name = "dateBtn";
            this.dateBtn.Size = new System.Drawing.Size(84, 36);
            this.dateBtn.TabIndex = 29;
            this.dateBtn.Text = "日期選擇";
            this.dateBtn.UseVisualStyleBackColor = true;
            this.dateBtn.Click += new System.EventHandler(this.dateBtn_Click);
            // 
            // dateBox
            // 
            this.dateBox.Location = new System.Drawing.Point(631, 13);
            this.dateBox.Name = "dateBox";
            this.dateBox.Size = new System.Drawing.Size(109, 25);
            this.dateBox.TabIndex = 28;
            // 
            // totalList
            // 
            this.totalList.BackColor = System.Drawing.SystemColors.Info;
            this.totalList.FormattingEnabled = true;
            this.totalList.ItemHeight = 15;
            this.totalList.Location = new System.Drawing.Point(27, 408);
            this.totalList.MultiColumn = true;
            this.totalList.Name = "totalList";
            this.totalList.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.totalList.Size = new System.Drawing.Size(805, 19);
            this.totalList.TabIndex = 27;
            // 
            // allViewList
            // 
            this.allViewList.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.allViewList.FormattingEnabled = true;
            this.allViewList.HorizontalScrollbar = true;
            this.allViewList.ItemHeight = 15;
            this.allViewList.Location = new System.Drawing.Point(27, 169);
            this.allViewList.Name = "allViewList";
            this.allViewList.ScrollAlwaysVisible = true;
            this.allViewList.Size = new System.Drawing.Size(806, 229);
            this.allViewList.TabIndex = 26;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.InType);
            this.groupBox2.Controls.Add(this.OutType);
            this.groupBox2.Location = new System.Drawing.Point(603, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 43);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "收入/支出";
            // 
            // InType
            // 
            this.InType.AutoSize = true;
            this.InType.Location = new System.Drawing.Point(0, 18);
            this.InType.Name = "InType";
            this.InType.Size = new System.Drawing.Size(58, 19);
            this.InType.TabIndex = 12;
            this.InType.TabStop = true;
            this.InType.Text = "收入";
            this.InType.UseVisualStyleBackColor = true;
            this.InType.CheckedChanged += new System.EventHandler(this.InType_CheckedChanged);
            // 
            // OutType
            // 
            this.OutType.AutoSize = true;
            this.OutType.Location = new System.Drawing.Point(123, 18);
            this.OutType.Name = "OutType";
            this.OutType.Size = new System.Drawing.Size(58, 19);
            this.OutType.TabIndex = 13;
            this.OutType.TabStop = true;
            this.OutType.Text = "支出";
            this.OutType.UseVisualStyleBackColor = true;
            // 
            // OkBtn
            // 
            this.OkBtn.Location = new System.Drawing.Point(521, 104);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(98, 35);
            this.OkBtn.TabIndex = 36;
            this.OkBtn.Text = "送出";
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // btn_export2
            // 
            this.btn_export2.Location = new System.Drawing.Point(631, 104);
            this.btn_export2.Name = "btn_export2";
            this.btn_export2.Size = new System.Drawing.Size(103, 35);
            this.btn_export2.TabIndex = 40;
            this.btn_export2.Text = "匯出";
            this.btn_export2.UseVisualStyleBackColor = true;
            this.btn_export2.Click += new System.EventHandler(this.btn_export2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 504);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "簡易資金管理工具";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cmbB_job4;
        private System.Windows.Forms.ComboBox cmbB_job3;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbB_job2;
        private System.Windows.Forms.ComboBox cmbB_job1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.TextBox txtb_cmp_phone;
        private System.Windows.Forms.TextBox txtb_cmp_email;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.TextBox txtb_address;
        private System.Windows.Forms.ComboBox cmb_address;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.TextBox txtb_name;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.TextBox txtb_good;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.ComboBox cmb_county;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.TextBox txtb_cmp_num;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtb_cmp_name;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton rdb_f;
        private System.Windows.Forms.RadioButton rdb_m;
        private System.Windows.Forms.ComboBox cmB_addcounty;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox explainBox;
        private System.Windows.Forms.TextBox moneyBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox detailCombo;
        private System.Windows.Forms.ComboBox CategoryCom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox taxPer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button dateBtn;
        private System.Windows.Forms.TextBox dateBox;
        private System.Windows.Forms.ListBox totalList;
        private System.Windows.Forms.ListBox allViewList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton InType;
        private System.Windows.Forms.RadioButton OutType;
        private System.Windows.Forms.CheckBox TaxEnabled;
        private System.Windows.Forms.TextBox customerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_export2;
    }
}

