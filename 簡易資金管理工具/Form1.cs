﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ClassTx;
using sIO;

// NPOI
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;


namespace 簡易資金管理工具
{
    
    public partial class Form1 : Form
    {
        string path = @"userdata.txt";
        SortedList allRecs = new SortedList();
        int latestIdx = 0;

        public string[] job1cmb = new string[30];
        public string[,] job2cmb = new string[100,5];
        public string[,] job3cmb = new string[1000,5];
        public string[,] job4cmb = new string[10000,5];
        public string[,] county = new string[30, 50];
        int[] county_num = new int[50];
        public int x1 = 0, x2 = 0, x3 = 0, x4 = 0 , count = 0;

        int totalIn = 0;
        int totalOut = 0;
        int total;
        bool flag = false;
 



        public Form1()
        {
            InitializeComponent();
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            //縣市
            if (File.Exists(@"county.txt"))
            {

                string tmp;
                StreamReader sr = new StreamReader("county.txt", System.Text.Encoding.Default);
                while ((tmp = sr.ReadLine()) != null)
                {
                    string[] s = new string[50];
                    s = tmp.Split('、');
                    county_num[count] = s.Length;
                    //cmB_addcounty.Items.Add(s.Length);
                    for (int i = 0; i < s.Length; i++)
                        county[count, i] = s[i];
                    cmb_county.Items.Add(s[0]);
                    cmb_address.Items.Add(s[0]);
                    count++;
                }

                sr.Close();
            }
            
            //行業類別conboBox

            if (File.Exists(@"job.txt"))
            {
                string tmp;
                string[] s;

                
                StreamReader sr = new StreamReader("job.txt", System.Text.Encoding.Default);
                while ((tmp = sr.ReadLine()) != null)
                {
                    int i;

                    s = tmp.Split(' ');
                    if (!int.TryParse(s[0], out i))
                    {
                        tmp = s[0] + " " + s[1];
                        x1 = Convert.ToInt32(s[0][0]) - 65;
                        cmbB_job1.Items.Add(tmp);
                        job1cmb[x1] = tmp;
                    }
                    else if (s[0].Length == 2)
                    {
                        tmp = s[0] + " " + s[1];
                        x2 = int.Parse(s[0]);
                        
                        job2cmb[x2, 0] = Convert.ToString(x1);
                        job2cmb[x2, 1] = tmp;
                    }
                    else if (s[0].Length == 3)
                    {
                        tmp = s[0] + " " + s[1];
                        x3 = int.Parse(s[0]);
                        job3cmb[x3, 0] = Convert.ToString(x2);
                        job3cmb[x3, 1] = tmp;
                    }
                    else if (s[0].Length == 4)
                    {
                        tmp = s[0] + " " + s[1];
                        x4 = int.Parse(s[0]);
                        job4cmb[x4, 0] = Convert.ToString(x3);
                        job4cmb[x4, 1] = tmp;


                    }
                }

                sr.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (Checkinput() == 0) return;

            StreamWriter sw = new StreamWriter(@"company_info.txt", false, System.Text.Encoding.Default);
            sw.WriteLine("公司基本資料");
            sw.WriteLine("{0} {1}",lbl1.Text,txtb_cmp_name.Text);
            sw.WriteLine("{0} {1}",lbl2.Text, txtb_cmp_num.Text);
            sw.WriteLine("{0} {1}",lbl3.Text, cmb_county.Text);
            sw.WriteLine("{0} {1}",lbl4.Text, txtb_good.Text);
            sw.WriteLine("{0} {1}",lbl5.Text, txtb_name.Text);
            if(rdb_f.Checked)
                sw.WriteLine("{0} {1}",lbl6.Text, rdb_f.Text);
            else sw.WriteLine("{0} {1}",lbl6.Text, rdb_m.Text );
            sw.WriteLine("{0} {1}",lbl7.Text, cmb_address.Text +" "+ cmB_addcounty.Text+" " + txtb_address.Text);
            sw.WriteLine("{0} {1}",lbl8.Text, txtb_cmp_email.Text);
            sw.WriteLine("{0} {1}",lbl9.Text, txtb_cmp_phone.Text);
            sw.WriteLine("行業別 {0}", cmbB_job4.Text);
            MessageBox.Show("儲存成功");
            sw.Close();
        }

        //檢查輸入
        public int Checkinput()
        {
            if (txtb_cmp_name.Text == "")
            {
                MessageBox.Show("請輸入公司名稱");
                return 0;
            }
            else if (txtb_cmp_num.Text == "")
            {
                MessageBox.Show("請輸入公司編號");
                return 0;
            }
            else if (cmb_county.Text == "")
            {
                MessageBox.Show("請選擇公司所在縣市");
                return 0;
            }
            else if (txtb_good.Text == "")
            {
                MessageBox.Show("請輸入主要產品/營業項目");
                return 0;
            }
            else if (txtb_name.Text == "")
            {
                MessageBox.Show("請輸入負責人姓名");
                return 0;
            }
            else if(!rdb_f.Checked && !rdb_m.Checked)
            {
                MessageBox.Show("請選擇負責人性別");
                return 0;
            }
            else if (cmb_address.Text == "")
            {
                MessageBox.Show("請選擇公司地址縣市");
                return 0;
            }
            else if (cmB_addcounty.Text == "")
            {
                MessageBox.Show("請選擇公司鄉鎮市區");
                return 0;
            }

            else if (txtb_address.Text == "")
            {
                MessageBox.Show("請輸入公司地址");
                return 0;
            }
            else if (txtb_cmp_email.Text == "")
            {
                MessageBox.Show("請輸入公司email");
                return 0;
            }
            else if (txtb_cmp_phone.Text == "")
            {
                MessageBox.Show("請選擇公司電話");
                return 0;
            }
            else if (cmbB_job4.Text == "")
            {
                MessageBox.Show("請選擇行業別");
                return 0;
            }
            else return 1;

            //return 0;
        }

        
        private void btn_export_Click(object sender, EventArgs e)
        {
            
                                    
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void moneyBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateBtn_Click(object sender, EventArgs e)
        {
            if (monthCalendar1.Visible == false)
            {
                monthCalendar1.Visible = true;
            }
            else
            {
                monthCalendar1.Visible = false;
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
               
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1 )
            {
                allRecs.Clear();
                allViewList.Items.Clear();
                TxHelper datehelp = new TxHelper();
                dateBox.Text = datehelp.GetTimeNow();

                taxPer.Text = "0.05";
                taxPer.Enabled = false;
                TaxEnabled.Checked = false;
                InType.Checked = true;


                if (File.Exists(path))
                {
                    IOHelper helper = new IOHelper();
                    
                    //======================================
                    string[] temp = new string[9];
                    string[] allrecords = File.ReadAllLines(path);
                     for (int j = 0; j<allrecords.Length;j++)
                    {
                        bool type;
                        if (allrecords[j] == "") break;
                        temp = allrecords[j].Split(' ');
                        if (temp[0] == "True") type = true;
                        else type = false;
                        Tx record = new Tx(type, int.Parse(temp[1]), int.Parse(temp[2]), int.Parse(temp[3]),
                            temp[4], temp[5], temp[6], temp[7], temp[8]);
                         allRecs.Add(record.id, record);
                    }
                   // label1.Text=allRecs.Count - 1+"";
                    Tx latestRec = (Tx)allRecs.GetByIndex(allRecs.Count-1);
                    latestIdx = latestRec.id;
                    string[] allData = new string[allRecs.Count];
                    int i = 0;
                    for (int j =0;j<allRecs.Count;j++)
                    {
                        Tx ele = (Tx)allRecs.GetByIndex(j);
                        string type;
                        if (ele.type) type = "收入";
                        else type = "支出";
                        allData[i] = ele.id + "\t"+ type + "\t" + ele.date + "\t" + ele.money + "\t" + ele.customer + "\t" + ele.category + "\t" + ele.detail + "\t" + ele.explain;
                        i++;
                    }
                    allViewList.Items.AddRange(allData);
                    //================================================
                    
                    for (int j = 0; j < allRecs.Count; j++)
                    {
                        Tx ele = (Tx)allRecs.GetByIndex(j);
                        if (ele.type == true) totalIn += ele.money;
                        else totalOut += ele.money;
                    }
                    total = totalIn - totalOut;
                    totalList.Items.Add("總金額:" + total);
                    totalList.Items.Add("總支出:" + totalOut);
                    totalList.Items.Add("總收入:" + totalIn);
                    //讀入txt加上讀出最後的index
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(path))
                    { sw.Close(); }
                    
                        latestIdx = 0;
                }

            }
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            dateBox.Text = monthCalendar1.SelectionStart.ToShortDateString();
        }

        private void InType_CheckedChanged(object sender, EventArgs e)
        {
            List<string> initdetails = new List<string>();
            TxHelper detailHelp = new TxHelper()
;
            if (InType.Checked == true)
            {
                CategoryCom.Items.Clear();
                CategoryCom.Text = "銷售";
                CategoryCom.Items.Add("銷售");
                CategoryCom.Items.Add("進貨退貨及折讓");
                CategoryCom.Items.Add("非營業收入");
                CategoryCom.Items.Add("銀行提現");
                CategoryCom.Items.Add("借款");
                CategoryCom.Items.Add("還款");
                CategoryCom.Items.Add("出售");
                CategoryCom.Items.Add("公司設立或增資");

                detailHelp.detailMemberIn(CategoryCom.Text, ref initdetails);
                detailCombo.Items.Clear();
                foreach (string ele in initdetails) detailCombo.Items.Add(ele);
            }
            else if (OutType.Checked == true)
            {
                CategoryCom.Items.Clear();
                CategoryCom.Text = "進貨";
                CategoryCom.Items.Add("進貨");
                CategoryCom.Items.Add("銷貨退回");
                CategoryCom.Items.Add("其他費用");
                CategoryCom.Items.Add("非營業支出");
                CategoryCom.Items.Add("償還借款");
                CategoryCom.Items.Add("借給同業");
                CategoryCom.Items.Add("存現");
                CategoryCom.Items.Add("買固定資產");

                detailHelp.detailMemOut(CategoryCom.Text, ref initdetails);
                detailCombo.Items.Clear();
                foreach (string ele in initdetails) detailCombo.Items.Add(ele);
            }
        }

        private void TaxEnabled_CheckStateChanged(object sender, EventArgs e)
        {
            if (TaxEnabled.Checked == true)
            {
                taxPer.Enabled = true;
            }
            else
            {
                taxPer.Enabled = false;
            }
        }

        private void CategoryCom_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxHelper comoboHelp = new TxHelper();
            List<string> details = new List<string>();
            if (InType.Checked == true) comoboHelp.detailMemberIn(CategoryCom.Text, ref details);
            else comoboHelp.detailMemOut(CategoryCom.Text, ref details);

            detailCombo.Text = "";
            detailCombo.Items.Clear();
            foreach (string ele in details)
            {
                detailCombo.Items.Add(ele);
            }
        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            IOHelper helper = new IOHelper();
            if (detailCombo.Text == "" || moneyBox.Text == "" || dateBox.Text == "" || customerBox.Text == "")
            {
                MessageBox.Show("請輸入完整資訊");
                return;
            }
            else
            {
                MessageBox.Show("新增成功");
            }
            if (TaxEnabled.Checked && ((CategoryCom.Text == "銷售") || (CategoryCom.Text == "進貨退貨及折讓")
                || (CategoryCom.Text == "非營業收入") || (CategoryCom.Text == "出售") || (CategoryCom.Text == "進貨")
                || (CategoryCom.Text == "銷貨退回") || (CategoryCom.Text == "買固定資產")) 
                &&!((CategoryCom.Text == "開立銷貨發票的營業稅")|| (CategoryCom.Text == "進貨發票上的營業稅")|| (CategoryCom.Text == "開立發票的營業稅")
                ||(CategoryCom.Text == "出售固定資產之營業稅") || (CategoryCom.Text == "購買固定資產所支付的營業稅")))
            {
                int tax = (int)(int.Parse(moneyBox.Text) * double.Parse(taxPer.Text));
                int money = int.Parse(moneyBox.Text) - tax;
                latestIdx++;
                Tx newRec1 = new Tx(InType.Checked, latestIdx, money, 0, CategoryCom.Text, detailCombo.Text, explainBox.Text,
                    customerBox.Text, dateBox.Text);
                string type;
                if (newRec1.type) type = "收入";
                else type = "支出";
                string recStr = newRec1.id + "\t" + type + "\t" + newRec1.date + "\t" + newRec1.money + "\t" + newRec1.customer + "\t" + newRec1.category + 
                    "\t" + newRec1.detail + "\t" + newRec1.explain;
                allViewList.Items.Add(recStr);
                allRecs.Add(newRec1.id, newRec1);
                
                //=========================================================
                latestIdx++;
                if (CategoryCom.Text == "銷售")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "開立銷貨發票的營業稅", explainBox.Text,
                  customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
                else if (CategoryCom.Text == "進貨退貨及折讓")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "進貨發票上的營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
                else if (CategoryCom.Text == "非營業收入")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "開立發票的營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
                else if (CategoryCom.Text == "出售")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "出售固定資產之營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
                else if (CategoryCom.Text == "進貨")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "開立銷貨發票的營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
               
                else if (CategoryCom.Text == "銷貨退回")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "原銷貨發票上的營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }
                else if (CategoryCom.Text == "買固定資產")
                {
                    Tx newRec2 = new Tx(InType.Checked, latestIdx, tax, 0, CategoryCom.Text, "購買固定資產所支付的營業稅", explainBox.Text,
                 customerBox.Text, dateBox.Text);
                    string type1;
                    if (newRec1.type) type1 = "收入";
                    else type1 = "支出";
                    string recStr1 = newRec2.id + "\t" + type1 + "\t" + newRec2.date + "\t" + newRec2.money + "\t" + newRec2.customer + "\t" + newRec2.category +
                        "\t" + newRec2.detail + "\t" + newRec2.explain;
                    allViewList.Items.Add(recStr1);
                    allRecs.Add(newRec2.id, newRec2);
                }

                helper.outALL(ref allRecs);
            }
            //==================================================================
            else
            {
                latestIdx++;
                Tx newRec = new Tx(InType.Checked, latestIdx, int.Parse(moneyBox.Text), 0, CategoryCom.Text, detailCombo.Text, explainBox.Text, customerBox.Text, dateBox.Text);
                allRecs.Add(newRec.id, newRec);
                string type;
                if (newRec.type) type = "收入";
                else type = "支出";
                string recStr = newRec.id + "\t" + type + "\t" + newRec.date + "\t" + newRec.money + "\t" + newRec.customer + "\t" + newRec.category + "\t" + newRec.detail + "\t" + newRec.explain;
                allViewList.Items.Add(recStr);
                helper.outALL(ref allRecs);
            }
            //=====================================================================
            totalList.Items.Clear();
            int totalIn = 0;
            int totalOut = 0;
            int total;
            for (int i = 0; i < allRecs.Count; i++)
            {
                Tx ele = (Tx)allRecs.GetByIndex(i);
                if (ele.type == true) totalIn += ele.money;
                else totalOut += ele.money;
            }
            total = totalIn - totalOut;
            
            totalList.Items.Add("總收入:" + totalIn);
            totalList.Items.Add("總支出:" + totalOut);
            totalList.Items.Add("總金額:" + total);

        }

        private void btn_export2_Click(object sender, EventArgs e)
        {
            //匯出excel
            //建立Excel 
            IWorkbook wb = new XSSFWorkbook();
            ISheet cmp_info = wb.CreateSheet("公司資料");//建立sheet
            ISheet user_data = wb.CreateSheet("日記帳管理");//建立sheet
            string[] s;
            if (File.Exists(@"company_info.txt"))
            {
                

                int count = 0;
                string tmp;
                StreamReader sr = new StreamReader("company_info.txt", System.Text.Encoding.Default);
                while ((tmp = sr.ReadLine()) != null)
                {
                    s = tmp.Split(' ');
                    int l = s.Length;

                    //利用sheet建立列
                    cmp_info.CreateRow(count);//建立第列
                    for (int i = 0; i < l; i++)
                    {
                        cmp_info.GetRow(count).CreateCell(i).SetCellValue(s[i]);
                        cmp_info.AutoSizeColumn(i);//自動調整欄寬
                    }

                    count++;
                }               
                sr.Close();
            }

            if (File.Exists(@"userdata.txt"))
            {
                user_data.CreateRow(0);
                user_data.GetRow(0).CreateCell(0).SetCellValue("收入/支出");
                user_data.GetRow(0).CreateCell(1).SetCellValue("編號");
                user_data.GetRow(0).CreateCell(2).SetCellValue("總金額");
                user_data.GetRow(0).CreateCell(3).SetCellValue("稅率");
                user_data.GetRow(0).CreateCell(4).SetCellValue("項目種類");
                user_data.GetRow(0).CreateCell(5).SetCellValue("項目細項說明");
                user_data.GetRow(0).CreateCell(6).SetCellValue("說明");
                user_data.GetRow(0).CreateCell(7).SetCellValue("廠商");
                user_data.GetRow(0).CreateCell(8).SetCellValue("日期");

                int count = 1;
                string tmp;
                StreamReader sr = new StreamReader("userdata.txt");
                while ((tmp = sr.ReadLine()) != null)
                {
                    s = tmp.Split(' ');
                    int l = s.Length;
                    user_data.CreateRow(count);//建立第列
                    if (s[0].Length==4)
                        user_data.GetRow(count).CreateCell(0).SetCellValue("收入");
                    else
                        user_data.GetRow(count).CreateCell(0).SetCellValue("支出");
                    //利用sheet建立列

                    for (int i = 1; i < l; i++)
                    {

                        user_data.GetRow(count).CreateCell(i).SetCellValue(s[i]);

                    }

                    count++;
                }
                count++;
                int total2 = total;
                int totalIn2 = totalIn;
                int totalOut2 = totalOut;
                user_data.CreateRow(count);//建立第列
                user_data.GetRow(count).CreateCell(0).SetCellValue(totalList.Items[0].ToString());
                //user_data.GetRow(count).CreateCell(1).SetCellValue(total2);

                count++;
                user_data.CreateRow(count);//建立第列
                user_data.GetRow(count).CreateCell(0).SetCellValue(totalList.Items[1].ToString());
               //user_data.GetRow(count).CreateCell(1).SetCellValue(totalIn2);

                count++;
                user_data.CreateRow(count);//建立第列
                user_data.GetRow(count).CreateCell(0).SetCellValue(totalList.Items[2].ToString());
               // user_data.GetRow(count).CreateCell(1).SetCellValue(totalOut2);
                for (int i = 0; i < 10; i++)
                {


                    user_data.AutoSizeColumn(i);//自動調整欄寬
                }

                sr.Close();
            }

            FileStream file = new FileStream(@"output.xlsx", FileMode.Create, FileAccess.Write);//產生檔案
            wb.Write(file);
            file.Close();
            MessageBox.Show("匯出成功");

        }

        private void cmb_address_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmB_addcounty.SelectedIndex = -1;
            cmB_addcounty.Items.Clear();

            string s = (string) cmb_address.SelectedItem;
            for(int i = 0; i < count; i++)
            {

                if (s == county[i, 0])
                {
                    for(int j = 1; j < county_num[i] ; j++)
                    {
                        cmB_addcounty.Items.Add(county[i,j]);
                    }
                }
            }

        }
        private void cmbB_job1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbB_job2.SelectedIndex = -1;
            cmbB_job3.SelectedIndex = -1;
            cmbB_job4.SelectedIndex = -1;
            cmbB_job2.Items.Clear();
            cmbB_job3.Items.Clear();
            cmbB_job4.Items.Clear();
            string[] s;
            string tmp = (string)cmbB_job1.SelectedItem;
            s = tmp.Split(' ');
            int x = Convert.ToInt32(s[0][0]) - 65;
            for (int i = 0; i < 100; i++)
                if (job2cmb[i, 0] == Convert.ToString(x))
                    cmbB_job2.Items.Add(job2cmb[i, 1]);
        }

        private void cmbB_job2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbB_job2.SelectedIndex != -1)
            {
                cmbB_job3.SelectedIndex = -1;
                cmbB_job4.SelectedIndex = -1;
                cmbB_job3.Items.Clear();
                cmbB_job4.Items.Clear();

                string[] s = new string[30];
                string tmp = (string)cmbB_job2.SelectedItem;
                s = tmp.Split(' ');
                int x = Convert.ToInt32(s[0]);
                for (int i = 0; i < 1000; i++)
                    if (job3cmb[i, 0] == Convert.ToString(x))
                        cmbB_job3.Items.Add(job3cmb[i, 1]);
            }
            
        }

        private void cmbB_job3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbB_job3.SelectedIndex != -1)
            {
                cmbB_job4.SelectedIndex = -1;
                cmbB_job4.Items.Clear();

                string[] s = new string[30];
                string tmp = (string)cmbB_job3.SelectedItem;
                s = tmp.Split(' ');
                int x = Convert.ToInt32(s[0]);
                for (int i = 0; i < 10000; i++)
                    if (job4cmb[i, 0] == Convert.ToString(x))
                        cmbB_job4.Items.Add(job4cmb[i, 1]);
            }
        }
    }
}
